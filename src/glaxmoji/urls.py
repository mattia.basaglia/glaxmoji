
from django.urls import path, re_path
from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("render/", views.render_emoji, name="render_emoji"),

    re_path("emoji/(?P<slug>[-0-9a-f]+)(?:_(?P<order>\d+))?.png", views.image, name="image"),
    re_path("emoji/(?P<slug>[-0-9a-f]+)/add_image/", views.add_image, name="add_image"),
    re_path("emoji/(?P<slug>[-0-9a-f]+)/toggle/", views.toggle_emoji, name="toggle_emoji"),
    re_path("emoji/(?P<slug>[-0-9a-f]+)/regenerate/", views.regenerate, name="regenerate"),
    re_path("emoji/(?P<slug>[-0-9a-f]+)/edit/", views.edit_emoji, name="edit_emoji"),
    re_path("download.tar.gz", views.generate_tarball, name="generate_tarball"),
]

