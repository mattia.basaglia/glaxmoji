from urllib.parse import quote_plus

from django.core.management import BaseCommand

from glaxmoji import models


class Command(BaseCommand):
    def handle(self, **options):
        group = models.EmojiGroup.objects.get(name="Flags")
        for emoji in models.Emoji.objects.filter(group__parent=group):
            genimg = emoji.images.filter(generator__api="flag.png").first()
            if genimg:
                genimg.generator.refresh_image()
            else:
                generator = models.ImageGenerator(
                    api="flag.png",
                    query_string="flag=%s" % quote_plus(emoji.emoji)
                )
                generator.for_emoji(emoji)
                generator.save()
