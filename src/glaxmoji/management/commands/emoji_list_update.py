import re
from urllib.request import urlopen

from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.core.management import BaseCommand, CommandError

from glaxmoji import models


class Command(BaseCommand):
    file = "https://unicode.org/Public/emoji/%s/emoji-test.txt"

    def add_arguments(self, parser):
        parser.add_argument(
            "--emoji-version",
            "-e",
            default="latest",
            help="Load a specific version of the Emoji standard"
        )

    def handle(self, emoji_version, **options):
        qualifiers = {
            "skin_tone": ["1F3FB", "1F3FC", "1F3FD", "1F3FE", "1F3FF"],
            "gender": ["200D 2640", "200D 2642"],
            "hair_style": ["1F9B0", "1F9B1" "1F9B3", "1F9B2"],
        }
        for name, quallist in qualifiers.items():
            qualifiers[name] = [" %s " % i for i in quallist]

        current_group = None
        current_subgroup = None
        group_order = 0
        subgroup_order = 0
        emoji_order = 0
        reparser = re.compile(
            r'(?P<group>^\s*# (?P<grp_sub>sub)?group:\s*(?P<grp_name>.*))|' +
            r'(?P<emoji>^(?P<seq>[0-9A-Fa-f ]+); (?P<status>[-a-z ]+)# (?P<unicode>[^ ]+) (?P<name>.+)$)'
        )

        uri = self.file % emoji_version
        r = urlopen(uri)
        if r.code != 200:
            raise CommandError("Could not fetch file: %s" % r.code)
        encoding = r.headers.get_content_charset()

        for line in r:
            decoded = line.decode(encoding).strip()
            match = reparser.match(decoded)
            if not match:
                continue
            if match.group("group"):
                try:
                    model = models.EmojiGroup.objects.filter(name=match.group("grp_name")).get()
                except (ObjectDoesNotExist, MultipleObjectsReturned):
                    model = models.EmojiGroup(name=match.group("grp_name"))

                if match.group("grp_sub"):
                    current_subgroup = model
                    model.order = subgroup_order
                    model.parent = current_group
                    subgroup_order += 1
                else:
                    current_group = model
                    model.order = group_order
                    model.parent = None
                    group_order += 1
                    subgroup_order = 0
                emoji_order = 0
                model.save()
            elif match.group("emoji"):
                try:
                    emoji = models.Emoji.objects.filter(emoji=match.group("unicode")).get()
                except (ObjectDoesNotExist, MultipleObjectsReturned):
                    emoji = models.Emoji(emoji=match.group("unicode"))

                emoji.name = match.group("name")
                emoji.group = current_subgroup
                emoji.order = emoji_order
                emoji.status = models.Emoji.Status[match.group("status").strip().replace("-", "_")].value

                seq = match.group("seq")
                for name, quallist in qualifiers.items():
                    for qualifier in quallist:
                        if qualifier in seq:
                            setattr(emoji, "flag_" + name, True)
                            break
                    else:
                        setattr(emoji, "flag_" + name, False)

                emoji_order += 1
                emoji.save()
