from urllib.parse import quote_plus

from django.core.management import BaseCommand

from glaxmoji import models


class Command(BaseCommand):
    def handle(self, **options):
        unistart = 0x1F550
        unirange = range(0x1f550, 0x1f55b+1)
        for tick in range(24):
            hours = (1+tick) % 12
            minutes = 30 * (tick >= 12)
            time = "%02i:%02i" % (hours, minutes)
            self.apply_time(models.Emoji.objects.get(emoji=chr(unistart+tick)), time)

    def apply_time(self, emoji, time):
        genimgqs = emoji.images.filter(generator__api="clock.png")
        self.do_apply_time(emoji, genimgqs, time, False)
        self.do_apply_time(emoji, genimgqs, time, True)

    def do_apply_time(self, emoji, genimgqs, time, sleepy):
        if sleepy:
            genimg = genimgqs.filter(generator__query_string__contains="sleepy").first()
        else:
            genimg = genimgqs.exclude(generator__query_string__contains="sleepy").first()

        if genimg:
            genimg.generator.refresh_image()
        else:
            generator = models.ImageGenerator(
                api="clock.png",
                query_string="time=%s" % quote_plus(time) + ("&sleepy" if sleepy else "")
            )
            generator.for_emoji(emoji)
            generator.save()
