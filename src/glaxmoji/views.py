import io

from django.shortcuts import render, get_object_or_404, reverse
from django.http.response import Http404, HttpResponseRedirect, JsonResponse, HttpResponse
from django.contrib.auth.decorators import login_required

import PIL
import regex

from . import models, forms


def emojifilter(qs):
    return qs.filter(
        flag_gender=False,
        flag_skin_tone=False,
        flag_hair_style=False,
        status=models.Emoji.Status.fully_qualified.value,
        visible=True,
    )


def index(request):
    groups = []

    total = 0
    imaged = 0

    for group in models.EmojiGroup.objects.filter(parent__isnull=True):
        subgroups = []
        g_total = 0
        g_imaged = 0
        for subgroup in group.children.all():
            emoji = [
                (e, list(e.images.all()))
                for e in emojifilter(subgroup.emoji)
            ]
            if emoji:
                sg_total = len(emoji)
                sg_imaged = sum(1 if l else 0 for e, l in emoji)
                g_total += sg_total
                g_imaged += sg_imaged
                subgroups.append({
                    "group": subgroup,
                    "items": emoji,
                    "total": sg_total,
                    "glaxed": sg_imaged,
                })
        if subgroups:
            total += g_total
            imaged += g_imaged
            groups.append({
                "group": group,
                "items": subgroups,
                "total": g_total,
                "glaxed": g_imaged,
                "percent": round(g_imaged / g_total * 100)
            })

    return render(request, "glaxmoji/index.html", {
        "groups": groups,
        "total": total,
        "glaxed": imaged,
        "percent": round(imaged / total * 100)
    })


def image(request, slug, order):
    emoji = get_object_or_404(models.Emoji, emoji=models.Emoji.slug2emoji(slug))
    if not order:
        order = 0
    else:
        try:
            order = int(order)
        except ValueError:
            raise Http404()
    image = get_object_or_404(models.EmojiImage, emoji=emoji, order=order)

    if not image.image:
        raise Http404()
    return HttpResponseRedirect(image.image.url())


@login_required
def add_image(request, slug):
    emoji = get_object_or_404(models.Emoji, emoji=models.Emoji.slug2emoji(slug))
    form = forms.ImageForm(request.POST, request.FILES)
    if form.is_valid():
        obj = form.save(False)
        obj.emoji = emoji
        obj.order = emoji.images.count()
        obj.save()
        return JsonResponse({"id": obj.id, "url": obj.image.url}, status=200)
    return JsonResponse({"error": form.errors}, status=400)


@login_required
def toggle_emoji(request, slug):
    emoji = get_object_or_404(models.Emoji, emoji=models.Emoji.slug2emoji(slug))
    form = forms.ToggleForm(request.POST, instance=emoji)
    if form.is_valid():
        form.save()
        return JsonResponse({}, status=200)
    return JsonResponse({"error": "invalid"}, status=400)


def render_emoji(request):
    default_size = 512
    unipoints = regex.findall(r"\X", request.GET.get("s", ""))
    images = []
    for u in unipoints:
        img = models.EmojiImage.objects.filter(emoji__emoji=u, order=0).first()
        if img:
            images.append(img)

    try:
        emosize = min(default_size, max(0, int(request.GET["sz"])))
    except (KeyError, ValueError):
        emosize = default_size

    img_fullw = default_size * len(images)

    try:
        img_width = int(request.GET["w"])
    except (KeyError, ValueError):
        img_width = emosize * len(images)

    try:
        img_height = min(default_size, max(0, int(request.GET["h"])))
    except (KeyError, ValueError):
        img_height = None

    img = PIL.Image.new("RGBA", (img_fullw, default_size))

    if emosize > 0 and img_width > 0 and len(images):
        x = 0
        for emojimg in images:
            img.paste(PIL.Image.open(emojimg.image), (x, 0))
            x += default_size

    if img_width < img_fullw:
        height = int(round(img_width / img_fullw * default_size))
        img = img.resize((img_width, height), PIL.Image.BILINEAR)
    else:
        height = default_size

    if img_height > height:
        canvas = PIL.Image.new("RGBA", (img_width, img_height))
        canvas.paste(img, (0, (img_height-height)//2))
        img = canvas

    buffer = io.BytesIO()
    img.save(buffer, format="PNG")
    return HttpResponse(buffer.getvalue(), "image/png")


@login_required
def regenerate(request, slug):
    emoji = get_object_or_404(models.Emoji, emoji=models.Emoji.slug2emoji(slug))
    for image in emoji.images.filter(generator__isnull=False):
        image.generator.refresh_image()
    return JsonResponse({}, status=200)


@login_required
def edit_emoji(request, slug):
    emoji = get_object_or_404(models.Emoji, emoji=models.Emoji.slug2emoji(slug))

    if request.POST:
        action = request.POST.get("action")
        image = get_object_or_404(emoji.images, id=request.POST.get("id"))
        if action == "mvup":
            image.order = max(image.order-1, 0)
            image.save()
            othimg = emoji.images.filter(order=image.order).exclude(id=image.id).first()
            if othimg:
                othimg.order = image.order + 1
                othimg.save()
        elif action == "mvdown":
            image.order = min(image.order+1, emoji.images.count()-1)
            image.save()
            othimg = emoji.images.filter(order=image.order).exclude(id=image.id).first()
            if othimg:
                othimg.order = image.order - 1
                othimg.save()
        elif action == "delete":
            order = image.order
            image.image.storage.delete(image.image.name)
            image.delete()
            for othimg in emoji.images.filter(order__gt=image.order):
                othimg.order = order
                othimg.save()
                order += 1
        elif action == "update":
            image.author = request.POST.get("author") or None
            image.save()
        elif action == "replace" and "image" in request.FILES:
            image.author = request.POST.get("author") or None
            image.replace_image(request.FILES["image"])
            image.save()

        return HttpResponseRedirect(reverse("edit_emoji", args=[slug]))

    return render(request, "glaxmoji/edit.html", {
        "emoji": emoji,
    })


def generate_tarball(request):
    import io
    import tarfile

    stream = io.BytesIO()
    with tarfile.open(None, "w:gz", stream) as tarf:
        for img in models.EmojiImage.objects.filter(order=0).order_by("emoji__emoji"):
            tarf.add(img.image.path, "glaxmoji/" + img.best_filename)
    resp = HttpResponse(stream.getvalue(), "application/tar+gzip")
    resp["Content-Disposition"] = "attachment; filename=glaxmoji.tar.gz"
    return resp

