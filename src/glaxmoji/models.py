import enum
from urllib.parse import urljoin
from urllib.request import urlopen

from django.db import models
from django.core.files.base import ContentFile
from django.conf import settings


class EmojiGroup(models.Model):
    parent = models.ForeignKey("EmojiGroup", on_delete=models.CASCADE,
                               null=True, db_index=True, related_name="children")
    name = models.CharField(max_length=64)
    order = models.PositiveSmallIntegerField()

    class Meta:
        ordering = ["order"]

    def __str__(self):
        if self.parent:
            return "%s :: %s" % (self.parent, self.name)
        return self.name


class Emoji(models.Model):
    class Status(enum.Enum):
        component = enum.auto()
        fully_qualified = enum.auto()
        minimally_qualified = enum.auto()
        unqualified = enum.auto()

    emoji = models.CharField(max_length=16, unique=True, db_index=True)
    name = models.CharField(max_length=128)
    group = models.ForeignKey(EmojiGroup, models.CASCADE, related_name="emoji")
    order = models.PositiveSmallIntegerField()
    status = models.PositiveSmallIntegerField(
        choices=[(st.value, st.name) for st in Status]
    )
    flag_gender = models.BooleanField(default=False)
    flag_skin_tone = models.BooleanField(default=False)
    flag_hair_style = models.BooleanField(default=False)
    visible = models.BooleanField(default=True)

    class Meta:
        ordering = ["order"]

    @staticmethod
    def hexord(char):
        return "%x" % ord(char)

    @property
    def hexes(self):
        return map(self.hexord, self.emoji)

    @property
    def slug(self):
        return "-".join(self.hexes)

    @property
    def hex_title(self):
        return " ".join(self.hexes)

    def __str__(self):
        return "%s - %s" % (self.emoji, self.hex_title)

    @staticmethod
    def slug2emoji(slug):
        try:
            return "".join(chr(int(p, 16)) for p in slug.split("-"))
        except:
            return ""

    def has_generator(self):
        return self.images.filter(generator__isnull=False).exists()


def image_rename(instance, filename):
    if instance.emoji:
        return "%s.png" % instance.emoji.slug
    return filename


class EmojiImage(models.Model):
    emoji = models.ForeignKey(Emoji, models.CASCADE, related_name="images")
    image = models.ImageField(null=True, upload_to=image_rename)
    order = models.PositiveSmallIntegerField()
    author = models.CharField(max_length=32, null=True, default=None, blank=True)

    class Meta:
        ordering = ["order"]

    @property
    def best_filename(self):
        return "%s.png" % self.emoji.slug

    def replace_image(self, fileobj):
        if self.image.name:
            self.image.storage.delete(self.image.name)
        self.image.save(self.best_filename, fileobj)


class ImageGenerator(models.Model):
    image = models.OneToOneField(EmojiImage, models.CASCADE, related_name="generator")
    api = models.CharField(max_length=16)
    query_string = models.CharField(max_length=256)

    def refresh_image(self):
        self.image.replace_image(self.image_file())
        self.image.save()

    def image_file(self):
        return ContentFile(urlopen(self.url).read())

    @property
    def url(self):
        return urljoin(settings.DRAGON_BEST_API, self.api) + "?" + self.query_string

    def for_emoji(self, emoji):
        if self.image_id:
            raise Exception("Already bound to an image object")
        self.image = EmojiImage(
            emoji=emoji,
            order=emoji.images.count()
        )
        self.image.image.save("%s.png" % emoji.slug, self.image_file())
        self.image.save()
        self.image_id = self.image.id
        return self.image

    def __str__(self):
        return self.api + "?" + self.query_string


