from django.apps import AppConfig


class GlaxmojiConfig(AppConfig):
    name = 'glaxmoji'
