from django.forms.models import modelform_factory

from . import models


ImageForm = modelform_factory(
    models.EmojiImage,
    fields=["image", "author"]
)

ToggleForm = modelform_factory(models.Emoji, fields=["visible"])
