from django.contrib import admin
from django.utils.html import format_html

from . import models


def set_title(callable, title):
    callable.short_description = title
    callable.__name__ = "lambda"
    return callable


admin.site.register(
    models.EmojiGroup,
    ordering=["parent__order", "order"]
)
admin.site.register(
    models.Emoji,
    ordering=["group__parent__order", "group__order", "order"],
    search_fields=["emoji", "name", "group__name"],
    list_display=('__str__', "name", "group", "visible"),
    list_filter=("visible", "status", "flag_gender", "flag_skin_tone", "flag_hair_style", "group"),
)

preview = set_title(lambda i: format_html("<img src='{}' style='width: 32px'/>", i.image.url), "Preview")
admin.site.register(
    models.EmojiImage,
    ordering=["emoji__group__parent__order", "emoji__group__order", "emoji__order", "order"],
    search_fields=["emoji__emoji", "emoji__name", "emoji__group__name"],
    list_display=(preview, "emoji", set_title(lambda i: i.emoji.group, "Group"),),
    list_filter=("author", "emoji__group",),
    list_display_links=[preview],
)

admin.site.register(
    models.ImageGenerator,
    search_fields=["image__emoji__emoji", "image__emoji__name", "image__emoji__group__name", "api", "query_string"],
    list_display=[
        "__str__",
        "api",
        "query_string",
        "url",
        set_title(lambda i: i.image.emoji, "Emoji"),
        set_title(lambda g: preview(g.image), "Preview")
    ],
    ordering=["image__emoji__group__parent__order", "image__emoji__group__order", "image__emoji__order", "image__order"],
    list_filter=("image__emoji__group",),

)
